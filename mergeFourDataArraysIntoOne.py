#!/usr/bin/env python2

from vtk import *
from sys import argv, exit

if len(argv) < 5:
    print "Usage:", argv[0], "input.vtu scalar_array1 ... name_for_new_array output.vtu"
    exit(1)

input_file = argv[1]
output_file = argv[7]
print("Reading from", input_file)
print("Writing to", output_file)

r = vtkXMLUnstructuredGridReader()
r.SetFileName(input_file)
r.Update()

m = r.GetOutput()
cell_data = m.GetCellData()

scalar1_array = cell_data.GetArray(argv[2])
scalar2_array = cell_data.GetArray(argv[3])
scalar3_array = cell_data.GetArray(argv[4])
scalar4_array = cell_data.GetArray(argv[5])
new_array = vtkDoubleArray()
new_array.SetNumberOfComponents(4)
new_array.SetNumberOfTuples(scalar1_array.GetNumberOfTuples())
new_array.SetName(argv[6])
new_array.CopyComponent(0, scalar1_array, 0)
new_array.CopyComponent(1, scalar2_array, 0)
new_array.CopyComponent(2, scalar3_array, 0)
new_array.CopyComponent(3, scalar4_array, 0)
cell_data.AddArray(new_array)
cell_data.RemoveArray(argv[2])
cell_data.RemoveArray(argv[3])
cell_data.RemoveArray(argv[4])
cell_data.RemoveArray(argv[5])

w = vtkXMLUnstructuredGridWriter()
w.SetFileName(output_file)
w.SetInputData(m)
w.SetDataModeToAscii()
w.SetCompressorTypeToNone()
w.Update()
