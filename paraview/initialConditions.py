# trace generated using paraview version 5.7.0
# removed unnecessary code

#### import the simple module from the paraview
from paraview.simple import *
import sys

# create a new 'XML Unstructured Grid Reader'
grid = XMLUnstructuredGridReader(FileName=sys.argv[1])

calculator_pressure = Calculator(Input=grid)
calculator_pressure.AttributeType = 'Point Data'
calculator_pressure.ResultArrayName = sys.argv[2]
calculator_pressure.Function = '-9810*(coordsZ-43.6715)'

calculator_temperature = Calculator(Input=calculator_pressure)
calculator_temperature.AttributeType = 'Point Data'
calculator_temperature.ResultArrayName = sys.argv[3]
calculator_temperature.Function = '278.15-0.03*(coordsZ-43.6715)'

# save data
SaveData(sys.argv[4], proxy=calculator_temperature)
