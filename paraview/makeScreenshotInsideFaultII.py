# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *

import sys

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Unstructured Grid Reader'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu = XMLUnstructuredGridReader(FileName=['/home/fischeth/tmp/cube_ht_benchmark/rev0/MPL_Modell/results/ConstViscosityThermalConvection_pcs_0_ts_9_t_0.601010.vtu'])
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu.CellArrayStatus = ['MaterialIDs']
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu.PointArrayStatus = ['T', 'darcy_velocity', 'initial_pressure', 'initial_temperature', 'p']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1452, 892]

# show data in view
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay = Show(constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu, renderView1)

# trace defaults for the display properties.
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.Representation = 'Surface'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.ColorArrayName = [None, '']
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OSPRayScaleArray = 'T'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.SelectOrientationVectors = 'None'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.ScaleFactor = 550.0
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.SelectScaleArray = 'None'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.GlyphType = 'Arrow'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.GlyphTableIndexArray = 'None'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.GaussianRadius = 27.5
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.SetScaleArray = ['POINTS', 'T']
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OpacityArray = ['POINTS', 'T']
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid = 'GridAxesRepresentation'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.SelectionCellLabelFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.SelectionPointLabelFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.PolarAxes = 'PolarAxesRepresentation'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.ScalarOpacityUnitDistance = 202.5684993194473

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.XTitleFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.YTitleFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.ZTitleFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.XLabelFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.YLabelFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.PolarAxes.PolarAxisTitleFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.PolarAxes.PolarAxisLabelFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtuDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Calculator'
calculator1 = Calculator(Input=constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.ResultArrayName = 'diff'
calculator1.Function = 'T-20+150/5500*coordsZ'

# show data in view
calculator1Display = Show(calculator1, renderView1)

# get color transfer function/color map for 'diff'
diffLUT = GetColorTransferFunction('diff')

# get opacity transfer function/opacity map for 'diff'
diffPWF = GetOpacityTransferFunction('diff')

# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = ['POINTS', 'diff']
calculator1Display.LookupTable = diffLUT
calculator1Display.OSPRayScaleArray = 'diff'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'None'
calculator1Display.ScaleFactor = 550.0
calculator1Display.SelectScaleArray = 'diff'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'diff'
calculator1Display.GaussianRadius = 27.5
calculator1Display.SetScaleArray = ['POINTS', 'diff']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'diff']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.SelectionCellLabelFontFile = ''
calculator1Display.SelectionPointLabelFontFile = ''
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityFunction = diffPWF
calculator1Display.ScalarOpacityUnitDistance = 202.5684993194473

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
calculator1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
calculator1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
calculator1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
calculator1Display.DataAxesGrid.XTitleFontFile = ''
calculator1Display.DataAxesGrid.YTitleFontFile = ''
calculator1Display.DataAxesGrid.ZTitleFontFile = ''
calculator1Display.DataAxesGrid.XLabelFontFile = ''
calculator1Display.DataAxesGrid.YLabelFontFile = ''
calculator1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1Display.PolarAxes.PolarAxisTitleFontFile = ''
calculator1Display.PolarAxes.PolarAxisLabelFontFile = ''
calculator1Display.PolarAxes.LastRadialAxisTextFontFile = ''
calculator1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(constViscosityThermalConvection_pcs_0_ts_9_t_0601010vtu, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Clip'
clip1 = Clip(Input=calculator1)
clip1.ClipType = 'Plane'
clip1.Scalars = ['POINTS', 'diff']
clip1.Value = 2.692843281693058e-08

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [2750.0, -2750.0, -2750.0]

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=clip1.ClipType)

# show data in view
clip1Display = Show(clip1, renderView1)

# trace defaults for the display properties.
clip1Display.Representation = 'Surface'
clip1Display.ColorArrayName = ['POINTS', 'diff']
clip1Display.LookupTable = diffLUT
clip1Display.OSPRayScaleArray = 'diff'
clip1Display.OSPRayScaleFunction = 'PiecewiseFunction'
clip1Display.SelectOrientationVectors = 'None'
clip1Display.ScaleFactor = 550.0
clip1Display.SelectScaleArray = 'diff'
clip1Display.GlyphType = 'Arrow'
clip1Display.GlyphTableIndexArray = 'diff'
clip1Display.GaussianRadius = 27.5
clip1Display.SetScaleArray = ['POINTS', 'diff']
clip1Display.ScaleTransferFunction = 'PiecewiseFunction'
clip1Display.OpacityArray = ['POINTS', 'diff']
clip1Display.OpacityTransferFunction = 'PiecewiseFunction'
clip1Display.DataAxesGrid = 'GridAxesRepresentation'
clip1Display.SelectionCellLabelFontFile = ''
clip1Display.SelectionPointLabelFontFile = ''
clip1Display.PolarAxes = 'PolarAxesRepresentation'
clip1Display.ScalarOpacityFunction = diffPWF
clip1Display.ScalarOpacityUnitDistance = 219.8325121339409

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
clip1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
clip1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
clip1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
clip1Display.DataAxesGrid.XTitleFontFile = ''
clip1Display.DataAxesGrid.YTitleFontFile = ''
clip1Display.DataAxesGrid.ZTitleFontFile = ''
clip1Display.DataAxesGrid.XLabelFontFile = ''
clip1Display.DataAxesGrid.YLabelFontFile = ''
clip1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
clip1Display.PolarAxes.PolarAxisTitleFontFile = ''
clip1Display.PolarAxes.PolarAxisLabelFontFile = ''
clip1Display.PolarAxes.LastRadialAxisTextFontFile = ''
clip1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(calculator1, renderView1)

# show color bar/color legend
clip1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# reset view to fit data
renderView1.ResetCamera()

# current camera placement for renderView1
renderView1.CameraPosition = [14546.715813032748, -2750.0, -2750.0]
renderView1.CameraFocalPoint = [1375.0, -2750.0, -2750.0]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 4125.0

# save screenshot
SaveScreenshot('/tmp/t.png', renderView1, ImageResolution=[1452, 892],
    OverrideColorPalette='WhiteBackground')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [14546.715813032748, -2750.0, -2750.0]
renderView1.CameraFocalPoint = [1375.0, -2750.0, -2750.0]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 4125.0

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).

sys.exit()
