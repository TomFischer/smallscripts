# trace generated using paraview version 5.6.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *

import sys

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Partitioned Unstructured Grid Reader'
unstructured_grid = XMLPartitionedUnstructuredGridReader(FileName=sys.argv[1])
unstructured_grid.CellArrayStatus = ['MaterialIDs']
unstructured_grid.PointArrayStatus = ['T', 'darcy_velocity', 'initial_pressure', 'initial_temperature', 'p']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1452, 892]

# show data in view
unstructured_gridDisplay = Show(unstructured_grid, renderView1)

# trace defaults for the display properties.
unstructured_gridDisplay.Representation = 'Surface'
unstructured_gridDisplay.ColorArrayName = [None, '']
unstructured_gridDisplay.OSPRayScaleArray = 'T'
unstructured_gridDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
unstructured_gridDisplay.SelectOrientationVectors = 'None'
unstructured_gridDisplay.ScaleFactor = 550.0
unstructured_gridDisplay.SelectScaleArray = 'None'
unstructured_gridDisplay.GlyphType = 'Arrow'
unstructured_gridDisplay.GlyphTableIndexArray = 'None'
unstructured_gridDisplay.GaussianRadius = 27.5
unstructured_gridDisplay.SetScaleArray = ['POINTS', 'T']
unstructured_gridDisplay.ScaleTransferFunction = 'PiecewiseFunction'
unstructured_gridDisplay.OpacityArray = ['POINTS', 'T']
unstructured_gridDisplay.OpacityTransferFunction = 'PiecewiseFunction'
unstructured_gridDisplay.DataAxesGrid = 'GridAxesRepresentation'
unstructured_gridDisplay.SelectionCellLabelFontFile = ''
unstructured_gridDisplay.SelectionPointLabelFontFile = ''
unstructured_gridDisplay.PolarAxes = 'PolarAxesRepresentation'
unstructured_gridDisplay.ScalarOpacityUnitDistance = 188.6907958081666

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
unstructured_gridDisplay.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
unstructured_gridDisplay.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
unstructured_gridDisplay.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
unstructured_gridDisplay.DataAxesGrid.XTitleFontFile = ''
unstructured_gridDisplay.DataAxesGrid.YTitleFontFile = ''
unstructured_gridDisplay.DataAxesGrid.ZTitleFontFile = ''
unstructured_gridDisplay.DataAxesGrid.XLabelFontFile = ''
unstructured_gridDisplay.DataAxesGrid.YLabelFontFile = ''
unstructured_gridDisplay.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
unstructured_gridDisplay.PolarAxes.PolarAxisTitleFontFile = ''
unstructured_gridDisplay.PolarAxes.PolarAxisLabelFontFile = ''
unstructured_gridDisplay.PolarAxes.LastRadialAxisTextFontFile = ''
unstructured_gridDisplay.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Calculator'
calculator1 = Calculator(Input=unstructured_grid)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.ResultArrayName = 'diff'
calculator1.Function = 'T-20+150/5500*coordsZ'

# show data in view
calculator1Display = Show(calculator1, renderView1)

# get color transfer function/color map for 'diff'
diffLUT = GetColorTransferFunction('diff')

# get opacity transfer function/opacity map for 'diff'
diffPWF = GetOpacityTransferFunction('diff')

# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = ['POINTS', 'diff']
calculator1Display.LookupTable = diffLUT
calculator1Display.OSPRayScaleArray = 'diff'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'None'
calculator1Display.ScaleFactor = 550.0
calculator1Display.SelectScaleArray = 'diff'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'diff'
calculator1Display.GaussianRadius = 27.5
calculator1Display.SetScaleArray = ['POINTS', 'diff']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'diff']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.SelectionCellLabelFontFile = ''
calculator1Display.SelectionPointLabelFontFile = ''
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityFunction = diffPWF
calculator1Display.ScalarOpacityUnitDistance = 188.6907958081666

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
calculator1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
calculator1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
calculator1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
calculator1Display.DataAxesGrid.XTitleFontFile = ''
calculator1Display.DataAxesGrid.YTitleFontFile = ''
calculator1Display.DataAxesGrid.ZTitleFontFile = ''
calculator1Display.DataAxesGrid.XLabelFontFile = ''
calculator1Display.DataAxesGrid.YLabelFontFile = ''
calculator1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
calculator1Display.PolarAxes.PolarAxisTitleFontFile = ''
calculator1Display.PolarAxes.PolarAxisLabelFontFile = ''
calculator1Display.PolarAxes.LastRadialAxisTextFontFile = ''
calculator1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(unstructured_grid, renderView1)

# show color bar/color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Clip'
clip1 = Clip(Input=calculator1)
clip1.ClipType = 'Plane'
clip1.Scalars = ['POINTS', 'diff']
clip1.Value = 0.045796192634469435

# init the 'Plane' selected for 'ClipType'
clip1.ClipType.Origin = [2750.0, -2750.0, -2750.0]

# toggle 3D widget visibility (only when running from the GUI)
Hide3DWidgets(proxy=clip1.ClipType)

# show data in view
clip1Display = Show(clip1, renderView1)

# trace defaults for the display properties.
clip1Display.Representation = 'Surface'
clip1Display.ColorArrayName = ['POINTS', 'diff']
clip1Display.LookupTable = diffLUT
clip1Display.OSPRayScaleArray = 'diff'
clip1Display.OSPRayScaleFunction = 'PiecewiseFunction'
clip1Display.SelectOrientationVectors = 'None'
clip1Display.ScaleFactor = 550.0
clip1Display.SelectScaleArray = 'diff'
clip1Display.GlyphType = 'Arrow'
clip1Display.GlyphTableIndexArray = 'diff'
clip1Display.GaussianRadius = 27.5
clip1Display.SetScaleArray = ['POINTS', 'diff']
clip1Display.ScaleTransferFunction = 'PiecewiseFunction'
clip1Display.OpacityArray = ['POINTS', 'diff']
clip1Display.OpacityTransferFunction = 'PiecewiseFunction'
clip1Display.DataAxesGrid = 'GridAxesRepresentation'
clip1Display.SelectionCellLabelFontFile = ''
clip1Display.SelectionPointLabelFontFile = ''
clip1Display.PolarAxes = 'PolarAxesRepresentation'
clip1Display.ScalarOpacityFunction = diffPWF
clip1Display.ScalarOpacityUnitDistance = 204.45655026704355

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
clip1Display.OSPRayScaleFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
clip1Display.ScaleTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
clip1Display.OpacityTransferFunction.Points = [-1.0, 0.0, 0.5, 0.0, 44.022716614425796, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
clip1Display.DataAxesGrid.XTitleFontFile = ''
clip1Display.DataAxesGrid.YTitleFontFile = ''
clip1Display.DataAxesGrid.ZTitleFontFile = ''
clip1Display.DataAxesGrid.XLabelFontFile = ''
clip1Display.DataAxesGrid.YLabelFontFile = ''
clip1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
clip1Display.PolarAxes.PolarAxisTitleFontFile = ''
clip1Display.PolarAxes.PolarAxisLabelFontFile = ''
clip1Display.PolarAxes.LastRadialAxisTextFontFile = ''
clip1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# hide data in view
Hide(calculator1, renderView1)

# show color bar/color legend
clip1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# reset view to fit data
renderView1.ResetCamera()

# current camera placement for renderView1
renderView1.CameraPosition = [17312.77613376963, -2750.0, -2750.0]
renderView1.CameraFocalPoint = [1375.0, -2750.0, -2750.0]
renderView1.CameraViewUp = [0.0, 0.0, 1.0]
renderView1.CameraParallelScale = 4125.0

# save screenshot
SaveScreenshot(sys.argv[2], renderView1, ImageResolution=[1452, 892],
    OverrideColorPalette='PrintBackground')

sys.exit()
