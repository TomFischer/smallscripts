#!/usr/bin/env bash

resolution=$1
number_of_layers=$2
layer_thickness=$3

GIS_input_data_dir=Deutschland_GIS
project_template_dir=${GIS_input_data_dir}
output_data_dir=GWF_Models_Germany

if [ ! -d "${output_data_dir}" ]; then
    mkdir ${output_data_dir}
fi

# STEP 1 		Raster2Mesh: Mesh the Domain with Elevation 
prefix=DeutschlandGebietFestland
fname_2D=${prefix}_${resolution}_2D
singularity exec --app ogs ogs-latest.sif Raster2Mesh -i ${GIS_input_data_dir}/DeutschlandGebietFestland_DEM_${resolution}m.asc -o ${output_data_dir}/${fname_2D}.vtu -p elevation -e tri
#
# STEP 2 		Include Material Information
singularity exec --app ogs ogs-latest.sif AssignRasterDataToMesh -i ${output_data_dir}/${fname_2D}.vtu -r ${GIS_input_data_dir}/Deutschland_MAT_BGR.asc -o ${output_data_dir}/${fname_2D}.vtu -c -s BGR_MaterialIDs
singularity exec --app ogs ogs-latest.sif convertVtkDataArrayToVtkDataArray -i ${output_data_dir}/${fname_2D}.vtu -o ${output_data_dir}/${fname_2D}.vtu -e BGR_MaterialIDs -n MaterialIDs -t int
singularity exec --app ogs ogs-latest.sif editMaterialID -c -i ${output_data_dir}/${fname_2D}.vtu -o ${output_data_dir}/${fname_2D}.vtu
#
# STEP 3 		Extend to 3D Mesh
fname_3D=${prefix}_${resolution}_3D_Surface_and_Layer
cd ${output_data_dir}
ln -s ${fname_2D}.vtu ${fname_3D}0.vtu
cd ..
for i in `seq 1 ${number_of_layers}`;
do
    singularity exec --app ogs ogs-latest.sif AddLayer --copy-material-ids --add-layer-on-bottom -t ${layer_thickness} -i ${output_data_dir}/${fname_3D}$(($i-1)).vtu -o ${output_data_dir}/${fname_3D}${i}.vtu
done
singularity exec --app ogs ogs-latest.sif removeMeshElements -i ${output_data_dir}/${fname_3D}${number_of_layers}.vtu -o ${output_data_dir}/DeutschlandGebietFestland_3D.vtu -t tri
#
# STEP 4 		Prepare Boundary Surface Condition - Geometry
singularity exec --app ogs ogs-latest.sif ExtractSurface -i ${output_data_dir}/DeutschlandGebietFestland_3D.vtu -o ${output_data_dir}/DeutschlandGebietFestland_SurfaceBC.vtu
#
# STEP 5 		Prepare Boundary Surface Condition - Recharge
singularity exec --app ogs ogs-latest.sif AssignRasterDataToMesh -i ${output_data_dir}/DeutschlandGebietFestland_SurfaceBC.vtu -r ${GIS_input_data_dir}/Deutschland_RECHARGE_BGR4OGS.asc -o ${output_data_dir}/BC_Recharge_Deutschland.vtu -c -s ogs_recharge
#
# STEP 6		Prepare Boundary Condition - Rivers
singularity exec --app ogs ogs-latest.sif AssignRasterDataToMesh -i ${output_data_dir}/DeutschlandGebietFestland_SurfaceBC.vtu -r ${GIS_input_data_dir}/DeutschlandGewaessernetz.asc -o ${output_data_dir}/BC_RiverLakes_Deutschland.vtu -c -s surfacewater
singularity exec --app ogs ogs-latest.sif AssignRasterDataToMesh -i ${output_data_dir}/BC_RiverLakes_Deutschland.vtu -r ${GIS_input_data_dir}/DeutschlandDEM.asc -o ${output_data_dir}/BC_RiverLakes_Deutschland.vtu -n -s elevation
singularity exec --app ogs ogs-latest.sif removeMeshElements -i ${output_data_dir}/BC_RiverLakes_Deutschland.vtu -o ${output_data_dir}/BC_RiverLakes_Deutschland.vtu --outside -n surfacewater --min-value 2.0 --max-value 200.0
#
# STEP 7 		Collect Model Data and prepare for OGS Simulation
model_input_dir=${output_data_dir}/01-${resolution}m-Layer${number_of_layers}
mkdir -p ${model_input_dir}
cp ${project_template_dir}/001-Model_DE_V1.prj ${model_input_dir}/
mv ${output_data_dir}/BC_Recharge_Deutschland.vtu ${model_input_dir}/BC_Recharge_Deutschland.vtu
mv ${output_data_dir}/BC_RiverLakes_Deutschland.vtu ${model_input_dir}/BC_RiverLakes_Deutschland.vtu
mv ${output_data_dir}/DeutschlandGebietFestland_3D.vtu ${model_input_dir}/DeutschlandGebietFestland_3D.vtu
#
# STEP 8 		OGS Simulation
#singularity exec --app ogs ogs-latest.sif ogs -o ${output_data_dir}/OGS-Output-Data_01 ${output_data_dir}/OGS-Input-Data_01/001_Model_DE_V1.prj
#
