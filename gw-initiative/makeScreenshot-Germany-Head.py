# trace generated using paraview version 5.8.1
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

import sys

# create a new 'XML Partitioned Unstructured Grid Reader'
unstructured_grid = XMLPartitionedUnstructuredGridReader(FileName=sys.argv[1])
unstructured_grid.CellArrayStatus = ['MaterialIDs']
unstructured_grid.PointArrayStatus = ['HydraulicFlow', 'head', 'v']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [839, 536]

# get layout
layout1 = GetLayout()

# show data in view
unstructured_gridDisplay = Show(unstructured_gridvtu, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
unstructured_gridDisplay.Representation = 'Surface'
unstructured_gridDisplay.ColorArrayName = [None, '']
unstructured_gridDisplay.OSPRayScaleArray = 'HydraulicFlow'
unstructured_gridDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
unstructured_gridDisplay.SelectOrientationVectors = 'HydraulicFlow'
unstructured_gridDisplay.ScaleFactor = 84915.0
unstructured_gridDisplay.SelectScaleArray = 'HydraulicFlow'
unstructured_gridDisplay.GlyphType = 'Arrow'
unstructured_gridDisplay.GlyphTableIndexArray = 'HydraulicFlow'
unstructured_gridDisplay.GaussianRadius = 4245.75
unstructured_gridDisplay.SetScaleArray = ['POINTS', 'HydraulicFlow']
unstructured_gridDisplay.ScaleTransferFunction = 'PiecewiseFunction'
unstructured_gridDisplay.OpacityArray = ['POINTS', 'HydraulicFlow']
unstructured_gridDisplay.OpacityTransferFunction = 'PiecewiseFunction'
unstructured_gridDisplay.DataAxesGrid = 'GridAxesRepresentation'
unstructured_gridDisplay.PolarAxes = 'PolarAxesRepresentation'
unstructured_gridDisplay.ScalarOpacityUnitDistance = 34859.76072311472

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
unstructured_gridDisplay.ScaleTransferFunction.Points = [-85.38046314958774, 0.0, 0.5, 0.0, 100.16962809409995, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
unstructured_gridDisplay.OpacityTransferFunction.Points = [-85.38046314958774, 0.0, 0.5, 0.0, 100.16962809409995, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(unstructured_gridDisplay, ('POINTS', 'head'))

# rescale color and/or opacity maps used to include current data range
unstructured_gridDisplay.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
unstructured_gridDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'head'
headLUT = GetColorTransferFunction('head')

# get opacity transfer function/opacity map for 'head'
headPWF = GetOpacityTransferFunction('head')

# current camera placement for renderView1
renderView1.CameraPosition = [597665.7557, 5658002.9612, 1697925.3533431857]
renderView1.CameraFocalPoint = [597665.7557, 5658002.9612, 865.5]
renderView1.CameraParallelScale = 531470.0069761698

# save screenshot
SaveScreenshot(sys.argv[2], renderView1, ImageResolution=[1452, 892],
    OverrideColorPalette='PrintBackground')

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [597665.7557, 5658002.9612, 1697925.3533431857]
renderView1.CameraFocalPoint = [597665.7557, 5658002.9612, 865.5]
renderView1.CameraParallelScale = 531470.0069761698

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
sys.exit()
