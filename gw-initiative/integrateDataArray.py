# trace generated using paraview version 5.9.1

#### import the simple module from the paraview
from paraview.simple import *

import sys

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Unstructured Grid Reader'
grid = XMLUnstructuredGridReader(FileName=[sys.argv[1]])
# grid = XMLUnstructuredGridReader(registrationName=[sys.argv[1]], FileName=[sys.argv[1]])

cell_array_status = 'specific_flux'
if len(sys.argv) > 3:
    cell_array_status = sys.argv[3]

grid.CellArrayStatus = ['bulk_element_ids', 'bulk_face_ids', cell_array_status]
grid.PointArrayStatus = ['bulk_node_ids', 'hydraulic_head']

# Properties modified on grid
grid.TimeArray = 'None'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
gridDisplay = Show(grid, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
gridDisplay.Representation = 'Surface'
gridDisplay.ColorArrayName = [None, '']
gridDisplay.SelectTCoordArray = 'None'
gridDisplay.SelectNormalArray = 'None'
gridDisplay.SelectTangentArray = 'None'
gridDisplay.OSPRayScaleArray = 'bulk_node_ids'
gridDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
gridDisplay.SelectOrientationVectors = 'None'
gridDisplay.ScaleFactor = 910.0
gridDisplay.SelectScaleArray = 'None'
gridDisplay.GlyphType = 'Arrow'
gridDisplay.GlyphTableIndexArray = 'None'
gridDisplay.GaussianRadius = 45.5
gridDisplay.SetScaleArray = ['POINTS', 'bulk_node_ids']
gridDisplay.ScaleTransferFunction = 'PiecewiseFunction'
gridDisplay.OpacityArray = ['POINTS', 'bulk_node_ids']
gridDisplay.OpacityTransferFunction = 'PiecewiseFunction'
gridDisplay.DataAxesGrid = 'GridAxesRepresentation'
gridDisplay.PolarAxes = 'PolarAxesRepresentation'
gridDisplay.ScalarOpacityUnitDistance = 803.6619745167019
gridDisplay.OpacityArrayName = ['POINTS', 'bulk_node_ids']

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
gridDisplay.ScaleTransferFunction.Points = [486630.0, 0.0, 0.5, 0.0, 523220.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
gridDisplay.OpacityTransferFunction.Points = [486630.0, 0.0, 0.5, 0.0, 523220.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [650.0, 5000.0, 10300.0]
renderView1.CameraFocalPoint = [650.0, 5000.0, 300.0]

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Integrate Variables'
integrateVariables1 = IntegrateVariables(registrationName='IntegrateVariables1', Input=grid)

# Create a new 'SpreadSheet View'
spreadSheetView1 = CreateView('SpreadSheetView')
spreadSheetView1.ColumnToSort = ''
spreadSheetView1.BlockSize = 1024

# show data in view
integrateVariables1Display = Show(integrateVariables1, spreadSheetView1, 'SpreadSheetRepresentation')

# get layout
layout1 = GetLayoutByName("Layout #1")

# add view to a layout so it's visible in UI
AssignViewToLayout(view=spreadSheetView1, layout=layout1, hint=0)

# Properties modified on spreadSheetView1
spreadSheetView1.FieldAssociation = 'Cell Data'

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Block Number', 'Cell ID']

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Block Number', 'Cell ID', 'Area']

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Block Number', 'Cell ID', 'Area', 'Cell Type']

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Block Number', 'Cell ID', 'Area', 'Cell Type', 'bulk_element_ids']

# Properties modified on spreadSheetView1
spreadSheetView1.HiddenColumnLabels = ['Block Number', 'Cell ID', 'Area', 'Cell Type', 'bulk_element_ids', 'bulk_face_ids']

# export view
ExportView(sys.argv[2], view=spreadSheetView1, RealNumberPrecision=10)

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(1180, 953)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [650.0, 5000.0, 10300.0]
renderView1.CameraFocalPoint = [650.0, 5000.0, 300.0]
renderView1.CameraParallelScale = 4554.39348322035

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
