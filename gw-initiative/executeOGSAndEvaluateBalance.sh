#!/usr/bin/env bash

data_dir=$1
results_dir_master=$1/results_master
results_dir_velocity_field_improvements=$1/results_velocity_field_improvements

mkdir ${results_dir_master}

$HOME/w/o/cr/bin/ogs_master ${data_dir}/LF.prj -o ${results_dir_master}

/opt/paraview/bin/pvpython ~/w/scripts/gw-initiative/integrateDataArray.py ${data_dir}/LandTransConceptualModel2D_NeumannSurface.vtu ${results_dir_master}/inflow.csv inflow
/opt/paraview/bin/pvpython ~/w/scripts/gw-initiative/integrateDataArray.py ${results_dir_master}/LandTransConceptualModel2D_DirichletSurface_ts_2_t_86400.000000.vtu ${results_dir_master}/outflow.csv specific_flux

inflow=`tail -n 1 ${results_dir_master}/inflow.csv`
outflow=`tail -n 1 ${results_dir_master}/outflow.csv`
abs_err=`calc "${inflow}${outflow}"`
rel_err=`calc "(${inflow}${outflow})/${inflow}"`
echo "[master] absolute error inflow-outflow: ${abs_err}"
echo "[master] relative error (inflow-outflow)/inflow: ${rel_err}"

mkdir ${results_dir_velocity_field_improvements}
$HOME/w/o/cr/bin/ogs_velocity_field_improvements ${data_dir}/LF.prj -o ${results_dir_velocity_field_improvements}

/opt/paraview/bin/pvpython ~/w/scripts/gw-initiative/integrateDataArray.py ${data_dir}/LandTransConceptualModel2D_NeumannSurface.vtu ${results_dir_velocity_field_improvements}/inflow.csv inflow
/opt/paraview/bin/pvpython ~/w/scripts/gw-initiative/integrateDataArray.py ${results_dir_velocity_field_improvements}/LandTransConceptualModel2D_DirichletSurface_ts_2_t_86400.000000.vtu ${results_dir_velocity_field_improvements}/outflow.csv specific_flux

inflow=`tail -n 1 ${results_dir_velocity_field_improvements}/inflow.csv`
outflow=`tail -n 1 ${results_dir_velocity_field_improvements}/outflow.csv`
abs_err=`calc "${inflow}${outflow}"`
rel_err=`calc "(${inflow}${outflow})/${inflow}"`
echo "[velocity_field_improvements] absolute error inflow-outflow: ${abs_err}"
echo "[velocity_field_improvements] relative error (inflow-outflow)/inflow: ${rel_err}"
