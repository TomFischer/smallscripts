#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import matplotlib.dates as mdates
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from datetime import timedelta, datetime

df = pd.read_csv(sys.argv[1], sep=',', parse_dates=[0], header=None, names=['date', 'recharge in mm/d'])

fig, ax = plt.subplots(figsize=(8, 5))

start=datetime.fromisoformat('2019-01-01')
end=datetime.fromisoformat('2019-01-31')
month_df=df[(start <= df['date']) & (df['date'] < end)]

ax.plot(df.iloc[:,0], df.iloc[:,1])

ax.set_xlabel(df.columns.values[0])
ax.set_ylabel(df.columns.values[1])
ax.grid() #True, linestyle='-.')

#print('time difference is: ' + str((df['date'][2] - df['date'][0])))
#print('time difference in seconds is: ' + str((df['date'][2] - df['date'][0]).total_seconds()))

print('<coords>')
for i in range(0, len(df['date'])-1):
    print(str((df['date'][i] - df['date'][0]).total_seconds()), end=' ')
print('</coords>')

print('<values>')
for i in range(0, len(df['recharge in mm/d'])-1):
    print(str(df['recharge in mm/d'][i]/(1000 * 86400)), end=' ')
print('</values>')
plt.show()

