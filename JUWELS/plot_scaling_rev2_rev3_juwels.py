#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df_rev2 = pd.read_csv(sys.argv[1],header='infer')
df_rev3 = pd.read_csv(sys.argv[2],header='infer')

plot_scaling_min=False
plot_scaling_max=False
plot_scaling_median=False
plot_scaling_mean=False

plot_scaling_execution=False
plot_scaling_calculation=False

for arg in sys.argv:
    if (arg in ("--min")):
        plot_scaling_min=True
    if (arg in ("--max")):
        plot_scaling_max=True
    if (arg in ("--median")):
        plot_scaling_median=True
    if (arg in ("--mean")):
        plot_scaling_mean=True
    if (arg in ("--execution")):
        plot_scaling_execution=True
    if (arg in ("--calculation")):
        plot_scaling_calculation=True

fig, ax = plt.subplots(figsize=(8, 5))

last_index=len(df_rev2.iloc[:,0])
upper_idealy = df_rev2.iloc[last_index-1,0]/df_rev2.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df_rev2.iloc[0,0] for x in idealy]
ax.plot(idealx, idealy, 'k-.', label='ideal (rev2)')

last_index=len(df_rev3.iloc[:,0])
upper_idealy = df_rev3.iloc[last_index-1,0]/df_rev3.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df_rev3.iloc[0,0] for x in idealy]
ax.plot(idealx, idealy, 'k--', label='ideal (rev3)')

# compute calculation time
df_rev2['calculation_time'] = df_rev2['execution_max'] - df_rev2['io_max_ts0'] - df_rev2['io_max_ts2027']
df_rev3['calculation_time'] = df_rev3['execution_max'] - df_rev3['io_max_ts0'] - df_rev3['io_max_ts25']

# group the data
grouped_df_rev2_min=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_rev2_mean=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_rev2_median=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_rev2_max=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.max)

grouped_df_rev3_min=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_rev3_mean=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_rev3_median=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_rev3_max=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.max)

def plotScaling(grouped_df, column_name, rev, mark='+'):
    s = grouped_df[column_name][0]
    p0 = grouped_df['#partitions'][0]
    if (column_name == 'execution_max'):
        l1 = 'calculations + IO (' + rev + ', $s = \\left[\\frac{t(' + str(p0) + ')}{t(N)}\\right]$)'
    else:
        l1 = 'calculations only (' + rev + ', $s = \\left[\\frac{t(' + str(p0) + ')}{t(N)}\\right]$)'
    ax.plot(grouped_df.iloc[:,0], [s / x for x in grouped_df[column_name]], marker=mark, label=l1)

# scaling based on mean
if (plot_scaling_mean):
    if (plot_scaling_execution):
        plotScaling(grouped_df_rev2_mean, 'execution_max', 'rev2')
        plotScaling(grouped_df_rev3_mean, 'execution_max', 'rev3', mark='o')

    if (plot_scaling_calculation):
        plotScaling(grouped_df_rev2_mean, 'calculation_time', 'rev2')
        plotScaling(grouped_df_rev3_mean, 'calculation_time', 'rev3', mark='o')

# scaling based on median
if (plot_scaling_median):
    if (plot_scaling_execution):
        plotScaling(grouped_df_rev2_median, 'execution_max', 'rev2')
        plotScaling(grouped_df_rev3_median, 'execution_max', 'rev3', mark='o')

    if (plot_scaling_calculation):
        plotScaling(grouped_df_rev2_median, 'calculation_time', 'rev2')
        plotScaling(grouped_df_rev3_median, 'calculation_time', 'rev3', mark='o')

# scaling based on min
if (plot_scaling_min):
    if (plot_scaling_execution):
        plotScaling(grouped_df_rev2_min, 'execution_max', 'rev2')
        plotScaling(grouped_df_rev3_min, 'execution_max', 'rev3', mark='o')

    if (plot_scaling_calculation):
        plotScaling(grouped_df_rev2_min, 'calculation_time', 'rev2')
        plotScaling(grouped_df_rev3_min, 'calculation_time', 'rev3', mark='o')

# scaling based on max
if (plot_scaling_max):
    if (plot_scaling_execution):
        plotScaling(grouped_df_rev2_max, 'execution_max', 'rev2')
        plotScaling(grouped_df_rev3_max, 'execution_max', 'rev3', mark='o')

    if (plot_scaling_calculation):
        plotScaling(grouped_df_rev2_max, 'calculation_time', 'rev2')
        plotScaling(grouped_df_rev3_max, 'calculation_time', 'rev3', mark='o')

ax.set_title('OGS-6 Strong Scalings for the Hydro-thermal Process in Fractured Cube rev2/rev3 Examples')

ax.legend()
ax.set_xlabel('number of MPI processes $[N]$')
ax.set_ylabel('scaling')
ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig('scaling_ht_rev2_rev3_juwels.png', transparent=True, dpi=300)

plt.show()

