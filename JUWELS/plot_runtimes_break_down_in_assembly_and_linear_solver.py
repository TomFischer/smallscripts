#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df = pd.read_csv(sys.argv[1],header='infer')

plot_mean=False
plot_median=False
plot_assembly_times=False
plot_linear_solver_times=False
filename='/tmp/t.pdf'

for arg in sys.argv:
    if (arg in ("-a", "--assembly")):
        plot_assembly_times=True
    if (arg in ("", "--linear_solver")):
        plot_linear_solver_times=True

    if (arg in ("--median")):
        plot_median=True
    if (arg in ("--mean")):
        plot_mean=True


# size for beamer slides
# fig, ax = plt.subplots(figsize=(8, 5))
# size for paper
fig, ax = plt.subplots(figsize=(8, 5))

last_index=len(df.iloc[:,0])-1
upper_idealy = df.iloc[last_index,0]/df.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df.iloc[0,0] for x in idealy]


grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_mean=df.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_median=df.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)


if (plot_assembly_times):
    ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['assembly'], linestyle='--', marker='', label='time for assembly (min)', color='b', alpha=0.4)

    if (plot_mean):
        ax.plot(grouped_df_mean.iloc[:,0], grouped_df_mean['assembly'], marker='', label='time for assembly (mean)')

    if (plot_median):
        ax.plot(grouped_df_median.iloc[:,0], grouped_df_median['assembly'], marker='', label='time for assembly (median)')

    ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['assembly'], linestyle=':', marker='', label='time for assembly (max)', color='b', alpha=0.4)
    # ax.fill_between(grouped_df_max.iloc[:,0], grouped_df_min['execution_max'], grouped_df_max['execution_max'], color='b', alpha=0.1)

if (plot_linear_solver_times):
    print(grouped_df_min)
    ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['linear solver'], linestyle='--', marker='', label='time for linear solver (min)', color='#ff7f0e', alpha=0.4)

    if (plot_mean):
        ax.plot(grouped_df_mean.iloc[:,0], grouped_df_mean['linear solver'], marker='', label='time for linear solver (mean)')

    if (plot_median):
        ax.plot(grouped_df_median.iloc[:,0], grouped_df_median['linear solver'], marker='', label='time for linear solver (median)')

    ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['linear solver'], linestyle=':', marker='', label='time for linear solver (max)', color='#ff7f0e', alpha=0.4)

    ax.fill_between(grouped_df_max.iloc[:,0], grouped_df_min['linear solver'], grouped_df_max['linear solver'], color='#ff7f0e', alpha=0.1)

grouped_df=df.groupby(['#partitions'], as_index=False)
print(grouped_df.describe()['assembly'])

# title for beamer presentations
# ax.set_title('OGS-6 Run Time for the Hydro-thermal Process in Fractured Cube Example rev3')
# title for paper
# ax.set_title('OGS-6 Run Time for the Hydro-thermal Process in Fractured Cube Example')
ax.legend()
#ax.set_xlabel(df.columns.values[0])
ax.set_xlabel('number of MPI processes $[N]$')
ax.set_ylabel('run time in seconds')
ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig(filename, transparent=True, dpi=300)

plt.show()

