#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df_rev2 = pd.read_csv(sys.argv[1],header='infer')
df_rev3 = pd.read_csv(sys.argv[2],header='infer')

# compute calculation time
df_rev2['calculation_time'] = df_rev2['execution_max'] - df_rev2['io_max_ts0'] - df_rev2['io_max_ts2027']
df_rev3['calculation_time'] = df_rev3['execution_max'] - df_rev3['io_max_ts0'] - df_rev3['io_max_ts25']
df_rev2['p_times_execution_time'] = df_rev2['#partitions'] * df_rev2['execution_max']
df_rev2['p_times_calculation_time'] = df_rev2['#partitions'] * df_rev2['calculation_time']
df_rev3['p_times_execution_time'] = df_rev3['#partitions'] * df_rev3['execution_max']
df_rev3['p_times_calculation_time'] = df_rev3['#partitions'] * df_rev3['calculation_time']

# group the data
grouped_df_rev2_min=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_rev2_mean=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_rev2_median=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_rev2_max=df_rev2.groupby(['#partitions'], as_index=False).aggregate(np.max)

grouped_df_rev3_min=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_rev3_mean=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_rev3_median=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_rev3_max=df_rev3.groupby(['#partitions'], as_index=False).aggregate(np.max)

def plotParallelEfficiency(grouped_df, column_name, rev, mark='+'):
    p0_t0=grouped_df[column_name][0]
    if (column_name == 'p_times_execution_time'):
        l1 = 'calculations + IO (' + rev + ')'
    else:
        l1 = 'calculations only (' + rev + ')'
    ax.plot(grouped_df['#partitions'], [p0_t0 / x for x in grouped_df[column_name]], marker=mark, label=l1)

fig, ax = plt.subplots(figsize=(8, 5))

plotParallelEfficiency(grouped_df_rev2_min, 'p_times_execution_time', 'rev2')
plotParallelEfficiency(grouped_df_rev2_min, 'p_times_calculation_time', 'rev2')
plotParallelEfficiency(grouped_df_rev3_min, 'p_times_execution_time', 'rev3')
plotParallelEfficiency(grouped_df_rev3_min, 'p_times_calculation_time', 'rev3')

#min_mpi_processes=df.iloc[0,0]
#p0_t0 = df.iloc[0,8] * min_mpi_processes
#ax.plot(df.iloc[:,0], [p0_t0 / x for x in df.iloc[:,8]*df.iloc[:,0]], marker='P', label='total simulation')

#p0_calculation_time = df.iloc[0,10] * min_mpi_processes
#df['p_times_calculation_time'] = df['#partitions'] * df['calculation_time']
#ax.plot(df.iloc[:,0], [p0_calculation_time / x for x in df['p_times_calculation_time']], marker='o', label='pure computations')

ax.set_title('OGS-6 Parallel Efficiency for the Hydro-thermal Process in Fractured Cube Example')
ax.legend()
ax.set_xlabel('number of MPI processes $[N]$')
min_mpi_processes=grouped_df_rev2_min['#partitions'][0]
ax.set_ylabel('parallel efficiency') # $\\left[\\frac{' + str(min_mpi_processes) + '\cdot t(' + str(min_mpi_processes) + ')}{N \cdot t(N)}\\right]$')

ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig('parallel_efficiency_juwels_comparison_rev2_rev3.png', transparent=True, dpi=300)

plt.show()

