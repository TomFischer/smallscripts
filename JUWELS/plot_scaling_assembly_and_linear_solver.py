#!/usr/bin/env python

# input of the form:
# #partitions,assembly,linear solver,id
# 768,157.787260,17332.371699,2301354
# 1536,83.240810,8398.566840,2301355
# 2400,60.476799,7253.388173,2301356
# 3072,51.010056,5325.239093,2301357
# 4608,44.224966,4496.259827,2301358
# 6144,41.330765,4056.889373,2301359
# 7680,36.556173,2654.857629,2301360
# 9216,38.572647,2757.246750,2301361
# 12288,42.086015,1950.862249,2301362

# usage
# python plot_scaling_assembly_and_linear_solver.py input_data.txt --assembly --linear_solver --max

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

SMALL_SIZE = 12
MEDIUM_SIZE = 14
BIGGER_SIZE = 16

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title
df = pd.read_csv(sys.argv[1],sep=',',header='infer')

plot_scaling_min=False
plot_scaling_max=False
plot_scaling_median=False
plot_scaling_mean=False

plot_assembly=False
plot_linear_solver=False

for arg in sys.argv:
    if (arg in ("-a", "--assembly")):
        plot_assembly=True
    if (arg in ("", "--linear_solver")):
        plot_linear_solver=True

    if (arg in ("--min")):
        plot_scaling_min=True
    if (arg in ("--max")):
        plot_scaling_max=True
    if (arg in ("--median")):
        plot_scaling_median=True
    if (arg in ("--mean")):
        plot_scaling_mean=True

fig, ax = plt.subplots(figsize=(8, 5))

last_index=len(df.iloc[:,0])-1
upper_idealy = df.iloc[last_index,0]/df.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df.iloc[0,0] for x in idealy]
ax.plot(idealx, idealy, 'k--', label='ideal')

# group the data
grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_mean=df.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_median=df.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)

def annotateScalingCurve(data_column_name, second_column_name, df):
    ax.annotate('#DOFs / MPI process', xy=(8000,4), xycoords='data', xytext=(8000,4),
               textcoords='data',
               arrowprops=dict(linestyle='-', color='green', facecolor='green', width=1, shrink=0.05),
               horizontalalignment='center',
               verticalalignment='top', color='green')
    s = df[data_column_name][0]
    for i in range(0, len(df)):
        annotation_text=str(int(110000000/df['#partitions'][i]))
        x=df['#partitions'][i]
        y=df[data_column_name][0]/df[data_column_name][i]
        yshift=-1.5
        if i == 0:
            yshift=1.5
        if i == 1 or i == 3:
            yshift=2.0
        ax.annotate(annotation_text, xy=(x, y),  xycoords='data',
                    xytext=(x, y+yshift), textcoords='data',
                    arrowprops=dict(arrowstyle='-', edgecolor='white', facecolor='white'),
                    horizontalalignment='center',
                    verticalalignment='top', color='green'
                    )

def doPlot(grouped_df, df, column_name, l1, l2):
    s = grouped_df[column_name][0]
    ax.plot(grouped_df.iloc[:,0], [s / x for x in grouped_df[column_name]], marker='+', label=l1)

# scaling based on mean
if (plot_scaling_mean):
    if (plot_assembly):
        doPlot(grouped_df_mean, df, 'assembly', 'assembly (by mean)', 'total simulation')

    if (plot_linear_solver):
        doPlot(grouped_df_mean, df, 'linear solver', 'linear solver (by mean)', 'linear solver')

# scaling based on median
if (plot_scaling_median):
    if (plot_assembly):
        doPlot(grouped_df_median, df, 'assembly', 'assembly (by median)', 'total simulation')

    if (plot_linear_solver):
        doPlot(grouped_df_median, df, 'linear solver', 'linear solver (by median)', 'calculation')

# scaling based on min
if (plot_scaling_min):
    if (plot_assembly):
        doPlot(grouped_df_min, df, 'assembly', 'assembly', 'total simulation')

    if (plot_linear_solver):
        doPlot(grouped_df_min, df, 'linear solver', 'linear solver', 'linear solver')

# scaling based on max
if (plot_scaling_max):
    print(grouped_df_max)
    if (plot_assembly):
        doPlot(grouped_df_max, df, 'assembly', 'assembly (by max)', 'total simulation')

    if (plot_linear_solver):
        doPlot(grouped_df_max, df, 'linear solver', 'linear solver (by max)', 'calculation')

ax.set_title('OGS-6 Strong Scaling for the Hydro-thermal Process in Fractured Cube Example')

ax.legend()
#ax.set_xlabel(df.columns.values[0])
ax.set_xlabel('number of MPI processes $[N]$')
ax.set_ylabel('scaling $\\left[\\frac{t(' + str(df.iloc[0,0]) + ')}{t(N)}\\right]$')
ax.grid(True, linestyle='-.')

annotateScalingCurve('assembly', 'linear solver', grouped_df_min)

fig.tight_layout()
fig.savefig('/tmp/scaling_ht_cube_juwels.png', transparent=True, dpi=300)
fig.savefig('/tmp/scaling_ht_cube_juwels.pdf', transparent=True, dpi=300)

plt.show()

