#!/usr/bin/env python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
import math

df = pd.read_csv(sys.argv[1],header='infer')

plot_mean=False
plot_median=False
plot_execution_times=False
plot_calculation_times=False
filename='calculation_time_ht_cube_rev3_min_max_median_juwels.pdf'

for arg in sys.argv:
    if (arg in ("-e", "--execution")):
        plot_execution_times=True
    if (arg in ("-c", "--calculation")):
        plot_calculation_times=True
    if (arg in ("--median")):
        plot_median=True
    if (arg in ("--mean")):
        plot_mean=True


# size for beamer slides
# fig, ax = plt.subplots(figsize=(8, 5))
# size for paper
fig, ax = plt.subplots(figsize=(8, 5))

last_index=len(df.iloc[:,0])-1
upper_idealy = df.iloc[last_index,0]/df.iloc[0,0]
idealy = np.linspace(1.0, upper_idealy)
idealx = [x * df.iloc[0,0] for x in idealy]

#ax.plot(df.iloc[:,0], df['execution_max'], marker='P', label='time for calculation + IO max')
#ax.plot(df.iloc[:,0], df['io_min_ts0'], marker='o', label='min output time step 0')
#ax.plot(df.iloc[:,0], df['io_max_ts0'], marker='P', label='max output time step 0')
#ax.plot(df.iloc[:,0], df['io_min_ts25'], marker='o', label='min output time step 25')
#ax.plot(df.iloc[:,0], df['io_max_ts25'], marker='P', label='max output time step 25')
#ax.plot(df.iloc[:,0], df['io_max_ts25']+df['io_max_ts0'], marker='P', label='sum max output time')

df['calculation_time'] = df['execution_max'] - df['io_max_ts0'] - df['io_max_ts25']

grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
grouped_df_mean=df.groupby(['#partitions'], as_index=False).aggregate(np.mean)
grouped_df_median=df.groupby(['#partitions'], as_index=False).aggregate(np.median)
grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)


if (plot_execution_times):
    #ax.plot(df.iloc[:,0], df['execution_max'], marker='o', linestyle='None', label='time for calculation + IOs')
    ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['execution_max'], linestyle='--', marker='', label='time for calculation + IO (min)', color='b', alpha=0.4)

    if (plot_mean):
        ax.plot(grouped_df_mean.iloc[:,0], grouped_df_mean['execution_max'], marker='', label='time for calculation + IO (mean)')

    if (plot_median):
        ax.plot(grouped_df_median.iloc[:,0], grouped_df_median['execution_max'], marker='', label='time for calculation + IO (median)')
    ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['execution_max'], linestyle=':', marker='', label='time for calculation + IO (max)', color='b', alpha=0.4)
    ax.fill_between(grouped_df_max.iloc[:,0], grouped_df_min['execution_max'], grouped_df_max['execution_max'], color='b', alpha=0.1)

if (plot_calculation_times):
    #ax.plot(df.iloc[:,0], df['calculation_time'], marker='o', linestyle='None', label='calculation times')
    ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['calculation_time'], linestyle='--', marker='', label='time for calculation (min)', color='#ff7f0e', alpha=0.4)

    if (plot_mean):
        ax.plot(grouped_df_mean.iloc[:,0], grouped_df_mean['calculation_time'], marker='', label='time for calculation (mean)')

    if (plot_median):
        ax.plot(grouped_df_median.iloc[:,0], grouped_df_median['calculation_time'], marker='', label='time for calculation (median)')

    ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['calculation_time'], linestyle=':', marker='', label='time for calculation (max)', color='#ff7f0e', alpha=0.4)

    ax.fill_between(grouped_df_max.iloc[:,0], grouped_df_min['calculation_time'], grouped_df_max['calculation_time'], color='#ff7f0e', alpha=0.1)

#grouped_df_min=df.groupby(['#partitions'], as_index=False).aggregate(np.min)
#ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['calculation_time'], marker='o', label='calculation time only min')
#ax.plot(grouped_df_min.iloc[:,0], grouped_df_min['execution_min'], marker='o', label='time for calculation + IO min')

#ax.plot(df.iloc[:,0], df['calculation_time'], marker='o', linestyle='None', label='calculation time onlys')
#ax.plot(grouped_df_mean.iloc[:,0], grouped_df_mean['calculation_time'], marker='o', label='calculation time only mean')

#grouped_df_max=df.groupby(['#partitions'], as_index=False).aggregate(np.max)
#ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['calculation_time'], marker='P', label='calculation time only max')
#ax.plot(grouped_df_max.iloc[:,0], grouped_df_max['execution_max'], marker='P', label='time for calculation + IO max')

grouped_df=df.groupby(['#partitions'], as_index=False)
print(grouped_df.describe()['calculation_time'])

# title for beamer presentations
# ax.set_title('OGS-6 Run Time for the Hydro-thermal Process in Fractured Cube Example rev3')
# title for paper
# ax.set_title('OGS-6 Run Time for the Hydro-thermal Process in Fractured Cube Example')
ax.legend()
#ax.set_xlabel(df.columns.values[0])
ax.set_xlabel('number of MPI processes $[N]$')
ax.set_ylabel('run time in seconds')
ax.grid(True, linestyle='-.')

fig.tight_layout()
fig.savefig(filename, transparent=True, dpi=300)

plt.show()

