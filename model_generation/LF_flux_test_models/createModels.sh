#!/usr/bin/env bash

base_data_dir=$1
project_file_template_dir=$2
mesh_name=$3

declare -a boundary_condition_domains=("left_boundary" "right_boundary"
    "part_of_left_boundary" "right_boundary"
    "part_of_left_boundary" "part_of_right_boundary"
    "left_boundary" "part_of_right_boundary"
    "top_boundary" "bottom_boundary"
    "top_boundary" "part_of_bottom_boundary"
    "part_of_top_boundary" "bottom_boundary"
    "part_of_top_boundary" "part_of_bottom_boundary"
    "front_boundary" "back_boundary"
    "part_of_front_boundary" "back_boundary"
    "front_boundary" "part_of_back_boundary"
    "part_of_front_boundary" "part_of_back_boundary"
    )
declare -a boundary_condition_types=("Dirichlet" "Neumann")

for (( i = 0 ; i < ${#boundary_condition_domains[@]} ; i=$i+2 ));
do
    for boundary_condition_type1 in "${boundary_condition_types[@]}"
    do
        for boundary_condition_type2 in "${boundary_condition_types[@]}"
        do
            if [ "${boundary_condition_type1}" == "Neumann" ] && [ "${boundary_condition_type2}" == "Neumann" ];
            then
                continue
            fi

            flow=${boundary_condition_domains[$i]}_to_${boundary_condition_domains[$(($i+1))]}
            model_sub_dir=$flow/${boundary_condition_type1}_${boundary_condition_type2}
            model_dir=$base_data_dir/$model_sub_dir
            mkdir -p $model_dir
            project_file=$3_${boundary_condition_type1}_${boundary_condition_type2}.prj
            cp $project_file_template_dir/LF_template.prj $model_dir/$project_file
            sed -i "s/MESH/$mesh_name/" $model_dir/$project_file
            sed -i "s/BOUNDARY1_BC_TYPE1_VALUE/$4/" $model_dir/$project_file
            sed -i "s/BOUNDARY2_BC_TYPE2_VALUE/$5/" $model_dir/$project_file
            sed -i "s/BOUNDARY1/${boundary_condition_domains[$i]}/" $model_dir/$project_file
            sed -i "s/BOUNDARY2/${boundary_condition_domains[$(($i+1))]}/" $model_dir/$project_file
            sed -i "s/BC_TYPE1/${boundary_condition_type1}/" $model_dir/$project_file
            sed -i "s/BC_TYPE2/${boundary_condition_type2}/" $model_dir/$project_file

            ln -s $base_data_dir/$3.vtu $model_dir/$3.vtu
            ln -s $base_data_dir/$3_entire_boundary.vtu $model_dir/$3_entire_boundary.vtu

            ln -s $base_data_dir/$3_${boundary_condition_domains[$i]}.vtu $model_dir/$3_${boundary_condition_domains[$i]}.vtu
            ln -s $base_data_dir/$3_${boundary_condition_domains[$(($i+1))]}.vtu $model_dir/$3_${boundary_condition_domains[$(($i+1))]}.vtu
            mkdir $model_dir/results
        done
    done
done
