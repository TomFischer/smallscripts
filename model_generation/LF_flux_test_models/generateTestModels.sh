#!/usr/bin/env bash

OGS_BIN=$HOME/w/o/cr/bin
SCRIPT_DIR=$HOME/w/scripts/model_generation/LF_flux_test_models

base_data_dir=$1
nx=$2
ny=$3
nz=$4

lx=1
ly=1
lz=1

declare -a element_types=("hex" "tet" "prism" "pyramid")

for element_type in "${element_types[@]}"
do
    number_of_elements=$(($nx * $ny * $nz))
    if [ "$element_type" == "tet" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 6))
    fi
    if [ "$element_type" == "prism" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 2))
    fi
    if [ "$element_type" == "pyramid" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 6))
    fi

    mesh_name=cuboid_${lx}x${ly}x${lz}_${element_type}_${number_of_elements}
    data_dir=$base_data_dir/$mesh_name
    mkdir -p $data_dir
    $OGS_BIN/generateStructuredMesh --lx $lx --ly $ly --lz $lz --nx $nx --ny $ny --nz $nz -e $element_type -o $data_dir/$mesh_name.vtu
    $SCRIPT_DIR/createBoundaries.sh $OGS_BIN $SCRIPT_DIR $data_dir/$mesh_name
    $SCRIPT_DIR/createModels.sh $data_dir $SCRIPT_DIR $mesh_name 1e7 1e6
done
