#!/usr/bin/env bash

OGS_BIN=$1
SCRIPT_DIR=$2

$OGS_BIN/ExtractBoundary -i $3.vtu -o $3_entire_boundary.vtu

$SCRIPT_DIR/createLeftBoundaries.sh $3
$SCRIPT_DIR/createRightBoundaries.sh $3
$SCRIPT_DIR/createTopBoundaries.sh $3
$SCRIPT_DIR/createBottomBoundaries.sh $3
$SCRIPT_DIR/createFrontBoundaries.sh $3
$SCRIPT_DIR/createBackBoundaries.sh $3
