#!/usr/bin/env bash

# geometric dimensions
nx=10000
ny=10000
nz=300
# number of subdivisions per space dimension
lx=100
ly=100
lz=3

current_dir=`pwd`
data_dir=~/tmp/flux_analysis

./generateTestModels.sh ${data_dir} ${lx} ${ly} ${lz} ${nx} ${ny} ${nz} |& tee /tmp/log_generate.txt
#./runTestModels.sh ${data_dir} ${lx} ${ly} ${lz} ${nx} ${ny} ${nz} |& tee /tmp/log_run.txt
#./calculateBalancesOfTestModels.sh ${data_dir} ${lx} ${ly} ${lz} ${nx} ${ny} ${nz} |& tee /tmp/log_balance.txt

#
#cd ${data_dir}
#~/w/scripts/model_generation/LF_flux_test_models/balance_overview.sh
#cd ${current_dir}
