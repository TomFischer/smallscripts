#!/usr/bin/env bash

output_dir=$1
mkdir -p ${output_dir}/

# generate the domain geometry
~/w/o/cr/bin/GenerateLandTransConceptualModelGeometry -o ${output_dir}/LandTransConceptualModel2D.gml -b 200 -d 5 -n 20

# generate gmsh input file
~/w/o/cr/bin/geometryToGmshGeo -i ${output_dir}/LandTransConceptualModel2D.gml --mesh_density_scaling_at_points 0.5 --max_points_in_quadtree_leaf 2 -o ${output_dir}/LandTransConceptualModel2D.geo

# use gmsh to construct the 2d mesh
gmsh -2 -algo meshadapt ${output_dir}/LandTransConceptualModel2D.geo -o ${output_dir}/LandTransConceptualModel2D.msh -format msh22

# convert gmsh mesh format to vtu
~/w/o/cr/bin/GMSH2OGS -i ${output_dir}/LandTransConceptualModel2D.msh -o ${output_dir}/LandTransConceptualModel2D.vtu

# reorder element nodes
~/w/o/cr/bin/NodeReordering -i ${output_dir}/LandTransConceptualModel2D.vtu -o ${output_dir}/LandTransConceptualModel2D.vtu -m 1

# remove the lines, keep the triangles from the vtu mesh
~/w/o/cr/bin/removeMeshElements -i ${output_dir}/LandTransConceptualModel2D.vtu -t line -o ${output_dir}/LandTransConceptualModel2D.vtu

# add subsurface layer
~/w/o/cr/bin/AddLayer --add-layer-on-bottom --copy-material-ids -t 10 -i ${output_dir}/LandTransConceptualModel2D.vtu -o ${output_dir}/LandTransConceptualModel3D_1.vtu
~/w/o/cr/bin/removeMeshElements -i ${output_dir}/LandTransConceptualModel3D_1.vtu -t tri -o ${output_dir}/LandTransConceptualModel3D_1.vtu
~/w/o/cr/bin/AddLayer --add-layer-on-bottom --copy-material-ids -t 20 -i ${output_dir}/LandTransConceptualModel3D_1.vtu -o ${output_dir}/LandTransConceptualModel3D_2.vtu
~/w/o/cr/bin/AddLayer --add-layer-on-bottom --copy-material-ids -t 100 -i ${output_dir}/LandTransConceptualModel3D_2.vtu -o ${output_dir}/LandTransConceptualModel3D_3.vtu
~/w/o/cr/bin/AddLayer --add-layer-on-bottom --copy-material-ids -t 170 -i ${output_dir}/LandTransConceptualModel3D_3.vtu -o ${output_dir}/LandTransConceptualModel3D.vtu

# generate boundary meshes
#~/w/o/cr/bin/constructMeshesFromGeometry -m ${output_dir}/LandTransConceptualModel3D.vtu -g ${output_dir}/LandTransConceptualModel2D.gml
#mv LandTransConceptualModel2D_*.vtu ${output_dir}/

# generate boundary meshes - second possibility
~/w/o/cr/bin/ExtractSurface -i ${output_dir}/LandTransConceptualModel3D.vtu -x 0.0 -y 0.0 -z -1.0 -o ${output_dir}/LandTransConceptualModel3D_TopSurface.vtu
## Neumann
~/w/o/cr/bin/ResetPropertiesInPolygonalRegion -m ${output_dir}/LandTransConceptualModel3D_TopSurface.vtu -n NeumannSurfaceRegion -i 1 -g ${output_dir}/LandTransConceptualModel2D.gml -p NeumannPolyline -o ${output_dir}/LandTransConceptualModel3D_NeumannBoundaryMarked.vtu --any_of
~/w/o/cr/bin/removeMeshElements -i ${output_dir}/LandTransConceptualModel3D_NeumannBoundaryMarked.vtu -n NeumannSurfaceRegion --property-value 0 -o ${output_dir}/LandTransConceptualModel2D_NeumannSurface.vtu
## Dirichlet
~/w/o/cr/bin/ResetPropertiesInPolygonalRegion -m ${output_dir}/LandTransConceptualModel3D_TopSurface.vtu -n DirichletSurfaceRegion -i 1 -g ${output_dir}/LandTransConceptualModel2D.gml -p DirichletPolyline -o ${output_dir}/LandTransConceptualModel3D_DirichletBoundaryMarked.vtu
~/w/o/cr/bin/removeMeshElements -i ${output_dir}/LandTransConceptualModel3D_DirichletBoundaryMarked.vtu -n DirichletSurfaceRegion --property-value 0 -o ${output_dir}/LandTransConceptualModel2D_DirichletSurface.vtu

# generate the values on the Neumann mesh
~/w/o/cr/bin/createMeshCellProperty -i ${output_dir}/LandTransConceptualModel2D_NeumannSurface.vtu -n inflow --scaling_value 3.170e-9 -o ${output_dir}/LandTransConceptualModel2D_NeumannSurface.vtu

# copy project file to output dir
cp ~/w/scripts/model_generation/LF_flux_test_models/LF_adaptive_template.prj ${output_dir}/LF.prj
