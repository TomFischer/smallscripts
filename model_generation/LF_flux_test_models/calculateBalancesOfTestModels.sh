#!/usr/bin/env bash

script_path=$HOME/w/scripts/paraview/
base_data_dir=$1

declare -a element_types=("hex" "tet" "prism" "pyramid")

nx=$2
ny=$3
nz=$4

lx=1
ly=1
lz=1

declare -a boundary_condition_domains=("left_boundary" "right_boundary"
    "part_of_left_boundary" "right_boundary"
    "part_of_left_boundary" "part_of_right_boundary"
    "left_boundary" "part_of_right_boundary"
    "top_boundary" "bottom_boundary"
    "top_boundary" "part_of_bottom_boundary"
    "part_of_top_boundary" "bottom_boundary"
    "part_of_top_boundary" "part_of_bottom_boundary"
    "front_boundary" "back_boundary"
    "part_of_front_boundary" "back_boundary"
    "front_boundary" "part_of_back_boundary"
    "part_of_front_boundary" "part_of_back_boundary"
    )
declare -a boundary_condition_types=("Dirichlet" "Neumann")

for element_type in "${element_types[@]}"
do
    number_of_elements=$(($nx * $ny * $nz))
    if [ "$element_type" == "tet" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 6))
    fi
    if [ "$element_type" == "prism" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 2))
    fi
    if [ "$element_type" == "pyramid" ]
    then
        number_of_elements=$(($nx * $ny * $nz * 6))
    fi

    mesh_name=cuboid_${lx}x${ly}x${lz}_${element_type}_${number_of_elements}
    data_dir=$base_data_dir/$mesh_name

    for (( i = 0 ; i < ${#boundary_condition_domains[@]} ; i=$i+2 ));
    do
        for boundary_condition_type1 in "${boundary_condition_types[@]}"
        do
            for boundary_condition_type2 in "${boundary_condition_types[@]}"
            do
                if [ "${boundary_condition_type1}" == "Neumann" ] && [ "${boundary_condition_type2}" == "Neumann" ];
                then
                    continue
                fi

                flow=${boundary_condition_domains[$i]}_to_${boundary_condition_domains[$(($i+1))]}
                model_sub_dir=$flow/${boundary_condition_type1}_${boundary_condition_type2}
                model_dir=$data_dir/$model_sub_dir
                project_file=${mesh_name}_${boundary_condition_type1}_${boundary_condition_type2}.prj
                pvpython $script_path/compute_balance_vtu.py $model_dir/results/${mesh_name}_entire_boundary_flux_${boundary_condition_domains[$i]}_${boundary_condition_domains[$(($i+1))]}_t_86400.000000.vtu $model_dir/results/balance.txt
            done
        done
    done
done
