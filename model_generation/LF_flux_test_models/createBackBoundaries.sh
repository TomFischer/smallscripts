#!/usr/bin/env bash

BIN=$HOME/w/o/cr/bin

$BIN/removeMeshElements -i $1_entire_boundary.vtu --x-min 0.0 --x-max 1.0 --y-min 0.0 --y-max 0.9 --z-min 0 --z-max 1 -o $1_back_boundary.vtu

## remove elements outside the middle area
$BIN/removeMeshElements -i $1_back_boundary.vtu --x-min 0.68 --x-max 1 --y-min 0 --y-max 1 --z-min 0 --z-max 1 -o /tmp/b.vtu
$BIN/removeMeshElements -i /tmp/b.vtu --x-min 0.0 --x-max 0.32  --y-min 0 --y-max 1 --z-min 0.0 --z-max 1 -o /tmp/c.vtu
$BIN/removeMeshElements -i /tmp/c.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.0 --z-max 0.32 -o /tmp/d.vtu
$BIN/removeMeshElements -i /tmp/d.vtu --x-min 0.0 --x-max 1 --y-min 0 --y-max 1 --z-min 0.68 --z-max 1 -o $1_part_of_back_boundary.vtu
