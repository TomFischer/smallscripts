#!/bin/bash

x=$1
y=$2
z=$3

max_distance=`calc sqrt\($1*$1+$2*$2+$3*$3\)`

bin_path=/home/fischeth/w/scripts/scripts_HeatStorageHamburg/
script=${bin_path}/identifySubdomainsForMesh.sh

${script} $max_distance ADM_B10_POINT1.vtu $x $y $z
${script} $max_distance Elbphil_BA_POINT4.vtu $x $y $z
${script} $max_distance Elbphil_BA_POINT3.vtu $x $y $z
${script} $max_distance Elbphil_BA_POINT2.vtu $x $y $z
${script} $max_distance Elbphil_BA_POINT1.vtu $x $y $z
${script} $max_distance ADM_B11_POINT2.vtu $x $y $z
${script} $max_distance ADM_B11_POINT1.vtu $x $y $z
${script} $max_distance ADM_B10_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_KT3_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_KT3_POINT1.vtu $x $y $z
${script} $max_distance Koehlbrand_KT2_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_KT2_POINT1.vtu $x $y $z
${script} $max_distance Koehlbrand_KT1_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_KT1_POINT1.vtu $x $y $z
${script} $max_distance Koehlbrand_BP5_POINT1.vtu $x $y $z
${script} $max_distance Koehlbrand_BP4_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_BP4_POINT1.vtu $x $y $z
${script} $max_distance HB_POINT3.vtu $x $y $z
${script} $max_distance HB_POINT2.vtu $x $y $z
${script} $max_distance HB_POINT1.vtu $x $y $z
${script} $max_distance Elbphil_BB_POINT5.vtu $x $y $z
${script} $max_distance Elbphil_BB_POINT4.vtu $x $y $z
${script} $max_distance Elbphil_BB_POINT3.vtu $x $y $z
${script} $max_distance Elbphil_BB_POINT2.vtu $x $y $z
${script} $max_distance Elbphil_BB_POINT1.vtu $x $y $z
${script} $max_distance Elbphil_BA_POINT5.vtu $x $y $z
${script} $max_distance PB_POINT5.vtu $x $y $z
${script} $max_distance PB_POINT4.vtu $x $y $z
${script} $max_distance PB_POINT3.vtu $x $y $z
${script} $max_distance PB_POINT2.vtu $x $y $z
${script} $max_distance PB_POINT1.vtu $x $y $z
${script} $max_distance Koehlbrand_KT4_POINT2.vtu $x $y $z
${script} $max_distance Koehlbrand_KT4_POINT1.vtu $x $y $z
${script} $max_distance Shell_B1_POINT5.vtu $x $y $z
${script} $max_distance Shell_B1_POINT4.vtu $x $y $z
${script} $max_distance Shell_B1_POINT3.vtu $x $y $z
${script} $max_distance Shell_B1_POINT2.vtu $x $y $z
${script} $max_distance Shell_B1_POINT1.vtu $x $y $z
${script} $max_distance Sasol_B9_POINT2.vtu $x $y $z
${script} $max_distance Sasol_B9_POINT1.vtu $x $y $z
${script} $max_distance Sasol_B2_POINT3.vtu $x $y $z
${script} $max_distance Sasol_B2_POINT2.vtu $x $y $z
${script} $max_distance Sasol_B2_POINT1.vtu $x $y $z
${script} $max_distance Sasol_B1_POINT3.vtu $x $y $z
${script} $max_distance Sasol_B1_POINT2.vtu $x $y $z
${script} $max_distance Sasol_B1_POINT1.vtu $x $y $z
${script} $max_distance Sasol_B11_POINT2.vtu $x $y $z
${script} $max_distance Sasol_B11_POINT1.vtu $x $y $z
${script} $max_distance Sasol_B10_POINT2.vtu $x $y $z
${script} $max_distance Sasol_B10_POINT1.vtu $x $y $z
