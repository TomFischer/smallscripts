#! /bin/bash

### constructModell.sh x_cell_size y_cell_size z_cell_size input_mesh

x_size=$1
y_size=$2
z_size=$3

dir=$1x$2x$3

# create directory for the model
mkdir $dir

# create mesh
~/w/o/cr/bin/Vtu2Grid -i $4 -o ${dir}/${dir}.vtu -x $x_size -y $y_size -z $z_size

# set initial condition in the mesh
pvpython ~/w/scripts/paraview/initialConditions.py ${dir}/${dir}.vtu p0 T0 ${dir}/${dir}_with_initial_conditions.vtu

# set/correct the bulk ids for the additional point meshes
~/w/scripts/scripts_HeatStorageHamburg/identifySubdomains.sh $x_size $y_size $z_size

# extract entire surface for boundary conditions
~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 0 -y 0 -z 0 -o ${dir}/${dir}_entire_surface.vtu

# extract top surface
~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 0 -y 0 -z -1 -o ${dir}/TopSurface.vtu

# extract bottom surface
~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 0 -y 0 -z 1 -o ${dir}/BottomSurface.vtu

~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 0 -y -1 -z 0 -o ${dir}/BC1.vtu

~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x -1 -y 0 -z 0 -o ${dir}/BC2.vtu

~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 0 -y 1 -z 0 -o ${dir}/BC4.vtu

~/w/o/cr/bin/ExtractSurface -i ${dir}/${dir}_with_initial_conditions.vtu -a 89 -x 1 -y 0 -z 0 -o ${dir}/BC5.vtu

cd $dir
ln -s ${dir}_with_initial_conditions.vtu SubsurfaceMesh.vtu
cd ..
