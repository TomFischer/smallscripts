#!/bin/bash

x=$3
y=$4
z=$5

base=$3x$4x$5
mesh=$base.vtu
~/w/o/cr/bin/identifySubdomains -m $base/$mesh -o $base/ -s $1 -- $2 |& tee /tmp/distances_output
# remove the first 2 lines
tail -n +3 /tmp/distances_output |& tee /tmp/distances_output_without_first_lines
# remove the last line
head -n -1 /tmp/distances_output_without_first_lines |& tee /tmp/distances
# sort
sort -n -k 8 /tmp/distances |& tee /tmp/sorted_distances
# extract the first line (lowest distance)
head -n 2 /tmp/sorted_distances |& tee /tmp/lowest_distances
# output the lowest distance only
#lowest_distances=`awk '{ print $8 }' /tmp/lowest_distances`

total=0
count=0
for i in $( awk '{ print $8; }' /tmp/lowest_distances )
    do
        total=$(echo $total + $i | bc)
             ((count++))
   done
eps=$(echo "scale=5; $total / $count" | bc)
echo "$2: eps is $eps"
~/w/o/cr/bin/identifySubdomains -m $base/$mesh -o $base/ -s ${eps} -- $2


