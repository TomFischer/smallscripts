#!/usr/bin/env bash

set -e

setup_environment() {
    local hostname=$(hostname)
    echo "executing workflow on ${hostname}"

    if [[ "${hostname}" == "frontend1" || "${hostname}" == "frontend2" ]]; then
        # load necessary modules
        source "${HOME}/w/o/s/scripts/env/eve/cli.sh"
    fi

    export PATH="${PATH}:${HOME}/w/o/build/release/bin"
}

partition_mesh() {
    local num_partitions=$1
    local mesh_name=$(basename "$2" .vtu)
    local output_path=$3
    shift 3
    local additional_vtu_files="$@"

    mkdir -p "${output_path}"

    if [ ! -e "${mesh_name}.mesh" ]; then
        partmesh --ogs2metis -i "${mesh_name}.vtu"
    fi

    if [ ! -e "${output_path}/${mesh_name}.mesh" ]; then
        ln -s "../${mesh_name}.mesh" "${output_path}/${mesh_name}.mesh"
    fi

    partmesh -m -n "${num_partitions}" -i "${mesh_name}" -o "${output_path}" -- ${additional_vtu_files}
}

# Function to extract mesh names from XML using xmlstarlet
extract_meshes_from_xml() {
    local xml_file=$1
    xmlstarlet sel -t -v "//OpenGeoSysProject/meshes/mesh" -n "${xml_file}" | sed 's/ //g'
}

# Command-line parsing function
parse_command_line() {
    output_path=""

    while getopts "o:" opt; do
        case $opt in
            o) output_path="$OPTARG" ;;
            *) echo "Invalid option"; exit 1 ;;
        esac
    done
    shift $((OPTIND -1))

    if [ "$#" -lt 2 ]; then
        echo "Usage: $0 [-o output_path] <number_of_partitions> <xml_file>"
        exit 1
    fi

    number_of_partitions=$1
    xml_file=$2

    # Use default output path if none is provided
    if [ -z "${output_path}" ]; then
        output_path="${number_of_partitions}"
    fi
}

# Set up the environment
setup_environment

# Parse command-line arguments
parse_command_line "$@"

# Extract mesh names from XML
mesh_list=$(extract_meshes_from_xml "${xml_file}")

# Extract the first mesh for partitioning and the rest as additional meshes
mesh_file=$(echo "${mesh_list}" | head -n 1)
additional_vtu_files=$(echo "${mesh_list}" | tail -n +2 | tr '\n' ' ')

# Call the function to partition the mesh
partition_mesh "${number_of_partitions}" "${mesh_file}" "${output_path}" ${additional_vtu_files}
