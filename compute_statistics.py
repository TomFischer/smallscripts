#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd
from scipy import stats

df = pd.read_csv(sys.argv[1], header=None)

print('sum: ' + str(df.iloc[:,0].sum()))
print('mean: ' + str(df.iloc[:,0].mean()))
print('deviation: ' + str(df.iloc[:,0].var()))
print('min: ' + str(df.iloc[:,0].min()))
print('max: ' + str(df.iloc[:,0].max()))

