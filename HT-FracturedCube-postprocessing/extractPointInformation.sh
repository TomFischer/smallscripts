#!/usr/bin/env bash

declare -a node_ids=("8245" "14999" "4272" "23649" "11848")
declare -a partitions=("2096" "2125" "2129" "2521" "1430")
ts=_ts_$1
timestring=_t_$2
output_prefix=$3
time=$4

base_name_convergence_improvement=gmres_jacobi_scaling_experiment_ConvergenceImprovementsIII
job_id_convergence_improvement=1593163

base_name_master=gmres_jacobi_scaling_experiment_masterIII
job_id_master=1593344

base_name=${base_name_master}
job_id=${job_id_master}

file_path=${base_name}.sh/${job_id}/ThermalConvection_pcs_0${ts}${timestring}

tar jxf ${base_name}${ts}.tar.bz2 ${file_path}_${partitions[0]}.vtu ${file_path}_${partitions[1]}.vtu ${file_path}_${partitions[2]}.vtu ${file_path}_${partitions[3]}.vtu ${file_path}_${partitions[4]}.vtu

pos=0
for p in "${partitions[@]}";
do
    value=`python $HOME/w/ogs-utils/post/extractPointInformation/extractPointInformationFromVTUUsingPointID.py ${file_path}_${p}.vtu ${node_ids[${pos}]} T`
    echo "${time} ${value}" >> ${output_prefix}_${node_ids[${pos}]}_T.csv
    ((pos+=1))
done
