#!/bin/python

import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import pylab
import pandas as pd

colors = plt.rcParams["axes.prop_cycle"]()

number_of_datasets=len(sys.argv)-1
dfs = []

for i in range(0,number_of_datasets):
    dfs.append(pd.read_csv(sys.argv[i+1], sep=' '))

fig, ax = plt.subplots(figsize=(8, 5))

for i in range(0,number_of_datasets):
    ax.plot(dfs[i].iloc[:,0], dfs[i].iloc[:,1], label=sys.argv[i+1], linewidth=1, alpha=0.25)

plt.gca().set_prop_cycle(None)

for i in range(0,number_of_datasets):
    ax.plot(dfs[i].iloc[:,0], dfs[i].iloc[:,1].rolling(window=50).mean(), linewidth=2, alpha=0.95)

ax.legend()
ax.grid() #True, linestyle='-.')
plt.show()

